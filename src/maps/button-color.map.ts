import type { TailwindColor } from "@/enum/tailwind-colors.enum";

const buttonColorMap = new Map<TailwindColor, string>([
  [
    "slate",
    "transition bg-slate-200 hover:bg-slate-100 active:bg-slate-300 shadow-slate-500/100",
  ],
  [
    "gray",
    "transition bg-gray-200 hover:bg-gray-100 active:bg-gray-300 shadow-gray-500/100",
  ],
  [
    "zinc",
    "transition bg-zinc-200 hover:bg-zinc-100 active:bg-zinc-300 shadow-zinc-500/100",
  ],
  [
    "neutral",
    "transition bg-neutral-200 hover:bg-neutral-100 active:bg-neutral-300 shadow-neutral-500/100",
  ],
  [
    "stone",
    "transition bg-stone-200 hover:bg-stone-100 active:bg-stone-300 shadow-stone-500/100",
  ],
  [
    "red",
    "transition bg-red-200 hover:bg-red-100 active:bg-red-300 shadow-red-500/100",
  ],
  [
    "orange",
    "transition bg-orange-200 hover:bg-orange-100 active:bg-orange-300 shadow-orange-500/100",
  ],
  [
    "amber",
    "transition bg-amber-200 hover:bg-amber-100 active:bg-amber-300 shadow-amber-500/100",
  ],
  [
    "yellow",
    "transition bg-yellow-200 hover:bg-yellow-100 active:bg-yellow-300 shadow-yellow-500/100",
  ],
  [
    "lime",
    "transition bg-lime-200 hover:bg-lime-100 active:bg-lime-300 shadow-lime-500/100",
  ],
  [
    "green",
    "transition bg-green-200 hover:bg-green-100 active:bg-green-300 shadow-green-500/100",
  ],
  [
    "emerald",
    "transition bg-emerald-200 hover:bg-emerald-100 active:bg-emerald-300 shadow-emerald-500/100",
  ],
  [
    "teal",
    "transition bg-teal-200 hover:bg-teal-100 active:bg-teal-300 shadow-teal-500/100",
  ],
  [
    "cyan",
    "transition bg-cyan-200 hover:bg-cyan-100 active:bg-cyan-300 shadow-cyan-500/100",
  ],
  [
    "sky",
    "transition bg-sky-200 hover:bg-sky-100 active:bg-sky-300 shadow-sky-500/100",
  ],
  [
    "blue",
    "transition bg-blue-200 hover:bg-blue-100 active:bg-blue-300 shadow-blue-500/100",
  ],
  [
    "indigo",
    "transition bg-indigo-200 hover:bg-indigo-100 active:bg-indigo-300 shadow-indigo-500/100",
  ],
  [
    "violet",
    "transition bg-violet-200 hover:bg-violet-100 active:bg-violet-300 shadow-violet-500/100",
  ],
  [
    "purple",
    "transition bg-purple-200 hover:bg-purple-100 active:bg-purple-300 shadow-purple-500/100",
  ],
  [
    "fuchsia",
    "transition bg-fuchsia-200 hover:bg-fuchsia-100 active:bg-fuchsia-300 shadow-fuchsia-500/100",
  ],
  [
    "pink",
    "transition bg-pink-200 hover:bg-pink-100 active:bg-pink-300 shadow-pink-500/100",
  ],
  [
    "rose",
    "transition bg-rose-200 hover:bg-rose-100 active:bg-rose-300 shadow-rose-500/100",
  ],
]);

const map = new Map<number, string>([[1, "asd"]]);

export { buttonColorMap };
