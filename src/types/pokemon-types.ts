export type AbilityType = {
  slot: number;
  is_hidden: boolean;
  ability: { name: string };
};

export type SpriteType = {
  back_default: string | null;
  back_female: string | null;
  back_shiny: string | null;
  back_shiny_female: string | null;
  front_default: string | null;
  front_female: string | null;
  front_shiny: string | null;
  front_shiny_female: string | null;
};

export type StatType = {
  base_stat: number;
  effort: number;
  stat: { name: string };
};

export type PokemonDetailsType = {
  id: number;
  name: string;
  abilities: AbilityType[];
  sprites: SpriteType;
  stats: StatType[];
  weight: number;
};

export type FavoriteType = {
  id: number;
  name: string;
  sprite: string;
};
