import axios from "axios";

const http = axios;
http.defaults.baseURL = "https://pokeapi.co/api/v2";

export { http };
