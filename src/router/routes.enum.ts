enum AuthRoutesEnum {
  AUTH = "auth",
  LOGIN = "login",
  REGISTER = "register",
}

enum MainRoutes {
  MAIN = "main",
  HOME = "home",
  POLYGON = "polygon",
  DETAILS = "pokemon",
  FAVORITES = "favorites",
}

enum AppModules {
  AUTH = "auth",
  MAIN = "main",
}

export { AuthRoutesEnum, MainRoutes, AppModules };
