import type { RouteRecordRaw } from "vue-router";
import { AppModules, AuthRoutesEnum } from "./routes.enum";
const AuthLayoutComponent = () =>
  import("../modules/auth/AuthLayoutComponent.vue");
const LoginComponent = () =>
  import("../modules/auth/components/views/LoginComponent.vue");

export const authRoutes: RouteRecordRaw[] = [
  {
    path: `/${AppModules.AUTH}`,
    name: AuthRoutesEnum.AUTH,
    component: AuthLayoutComponent,
    redirect: { name: AuthRoutesEnum.LOGIN },
    children: [
      {
        path: AuthRoutesEnum.LOGIN,
        name: AuthRoutesEnum.LOGIN,
        component: LoginComponent,
      },
      {
        path: AuthRoutesEnum.REGISTER,
        name: AuthRoutesEnum.REGISTER,
        component: LoginComponent,
      },
    ],
  },
];
