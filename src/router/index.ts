import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import { authRoutes } from "./auth-routes";
import { mainRoutes } from "./main-routes";
import { AuthRoutesEnum } from "./routes.enum";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "homes",
      component: HomeView,
      redirect: { name: AuthRoutesEnum.AUTH },
      children: [...authRoutes, ...mainRoutes],
    },
  ],
});

export default router;
