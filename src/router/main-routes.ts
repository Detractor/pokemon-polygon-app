import type { RouteRecordRaw } from "vue-router";
const MainLayoutComponent = () =>
  import("../modules/main/MainLayoutComponent.vue");
const HomeComponent = () => import("../modules/main/views/HomeComponent.vue");
const PolygonComponent = () =>
  import("../modules/main/views/PolygonComponent.vue");
const PokemonDetailsComponent = () =>
  import("../modules/main/views/PokemonDetailsComponent.vue");
const FavoritesComponent = () =>
  import("../modules/main/views/FavoritesComponent.vue");

import { AppModules, MainRoutes } from "./routes.enum";
export const mainRoutes: RouteRecordRaw[] = [
  {
    path: `/${AppModules.MAIN}`,
    name: AppModules.MAIN,
    component: MainLayoutComponent,
    redirect: { name: MainRoutes.HOME },
    children: [
      {
        path: MainRoutes.HOME,
        name: MainRoutes.HOME,
        component: HomeComponent,
      },
      {
        path: `${MainRoutes.DETAILS}/:id`,
        name: MainRoutes.DETAILS,
        component: PokemonDetailsComponent,
      },
      {
        path: MainRoutes.POLYGON,
        name: MainRoutes.POLYGON,
        component: PolygonComponent,
      },
      {
        path: MainRoutes.FAVORITES,
        name: MainRoutes.FAVORITES,
        component: FavoritesComponent,
      },
    ],
  },
];
