import type { LoginCredentials } from "@/dtos/auth/login-credentials.dto";
import type { LoginResponse } from "@/dtos/auth/login-response.dto";
import type { AuthApiInterface } from "@/interfaces/auth-api.interface";
import { User } from "@/models/user.model";

export class MockAuthApi implements AuthApiInterface {
  private tokenKey = "akkshdu12938u";
  getSessionToken(): string | undefined {
    const sessionData = localStorage.getItem(this.tokenKey);
    if (!sessionData) return;
    const login: LoginResponse = JSON.parse(sessionData);

    return login.token;
  }

  getLoggedUser(): User | undefined {
    const sessionData = localStorage.getItem(this.tokenKey);
    if (!sessionData) return;
    const login: LoginResponse = JSON.parse(sessionData);

    return login.user;
  }

  login(credentials: LoginCredentials): Promise<LoginResponse> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (
          credentials.email === "trainer@pokemon.com" &&
          credentials.password === "CatchEmAll"
        ) {
          const response: LoginResponse = {
            token: this.makeRandomToken(20),
            expiration: new Date(),
            user: new User({
              age: 32,
              creationDate: new Date("09-10-1991 05:00:00"),
              email: credentials.email,
              firstName: "Andrés",
              lastName: "Bandera",
              likes: [],
            }),
          };
          this.persistSession(response);
          resolve(response);
        } else reject();
      }, 4000);
    });
  }
  logout(): Promise<boolean> {
    localStorage.removeItem(this.tokenKey);
    return new Promise((resolve) => resolve(true));
  }

  persistSession(response: LoginResponse): void {
    const responseString = JSON.stringify(response);
    localStorage.setItem(this.tokenKey, responseString);
  }

  private makeRandomToken(length: number): string {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }
}
