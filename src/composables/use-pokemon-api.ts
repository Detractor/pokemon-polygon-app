import type { PokemonApiInterface } from "@/interfaces/pokemon-api.interface";
import { http } from "@/http";
import { PokemonResponse } from "@/dtos/pokemon/pokemon-response.dto";
import type { PokemonDetailsType } from "@/types/pokemon-types";

export class PokemonApi implements PokemonApiInterface {
  private SUB_ROUTE = "/pokemon";
  async getPokemons(page: number): Promise<PokemonResponse> {
    const response = await http.get(
      `${this.SUB_ROUTE}?offset=${20 * page}&limit=20`
    );
    return new PokemonResponse(response.data);
  }

  async getPokemon(pokemonId: number): Promise<PokemonDetailsType> {
    const response = await http.get(`${this.SUB_ROUTE}/${pokemonId}`);
    return response.data;
  }
}
