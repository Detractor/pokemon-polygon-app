import type { Pokemon } from "@/models/pokemon.model";

const getFavorites = (): Pokemon[] => {
  const favorites = localStorage.getItem("favorites");
  let favoritesArray: Pokemon[] = [];
  if (favorites) favoritesArray = JSON.parse(favorites);
  else favoritesArray = [];

  return favoritesArray;
};

const isInFavorites = (id: number): boolean => {
  return getFavorites()
    .map((poke) => poke.id)
    .includes(id);
};

const addToFavorites = (pokemon: Pokemon): void => {
  const favorites = getFavorites();
  favorites.push(pokemon);
  localStorage.setItem("favorites", JSON.stringify(favorites));
};

const removeFromFavorites = (id: number): void => {
  const favorites = getFavorites();
  console.log("REMOVING", favorites);
  const index = favorites.findIndex((val) => val.id === id);
  favorites.splice(index, 1);
  localStorage.setItem("favorites", JSON.stringify(favorites));
};

export const usePokemonStore = () => {
  return {
    getFavorites,
    isInFavorites,
    removeFromFavorites,
    addToFavorites,
  };
};
