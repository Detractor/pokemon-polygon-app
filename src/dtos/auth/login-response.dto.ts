import type { User } from "@/models/user.model";

export type LoginResponse = {
  token: string;
  user: User;
  expiration: Date;
};
