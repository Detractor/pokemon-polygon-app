import { Pokemon } from "@/models/pokemon.model";

export class PokemonResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Pokemon[];

  constructor(response: PokemonResponse) {
    this.count = response.count;
    this.next = response.next;
    this.previous = response.previous;
    this.results = Pokemon.fromArray(response.results);
  }
}
