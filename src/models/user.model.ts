export class User {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  creationDate: Date;
  likes: any[];

  constructor(user: User) {
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.age = user.age;
    this.email = user.email;
    this.creationDate = user.creationDate;
    this.likes = user.likes;
  }
}
