export class PolygonVertex {
  x: number;
  y: number;
  constructor(vertex: PolygonVertex) {
    this.x = vertex.x;
    this.y = vertex.y;
  }
}
