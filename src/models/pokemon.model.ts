export class Pokemon {
  name: string;
  url: string;
  id: number;

  constructor(pokemon: Pokemon) {
    this.name = pokemon.name;
    this.url = pokemon.url;
    this.id = this.parseIdFromUrl();
  }

  private parseIdFromUrl(): number {
    const url = this.url.substring(0, this.url.length - 1);
    const id = url.substring(url.lastIndexOf("/") + 1);
    return Number(id);
  }

  public static fromArray(pokemons: Pokemon[]): Pokemon[] {
    return pokemons.map((poke) => new Pokemon(poke));
  }
}
