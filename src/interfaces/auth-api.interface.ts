import type { LoginCredentials } from "@/dtos/auth/login-credentials.dto";
import type { LoginResponse } from "@/dtos/auth/login-response.dto";
import type { User } from "@/models/user.model";

export interface AuthApiInterface {
  login(credentials: LoginCredentials): Promise<LoginResponse>;
  logout(): Promise<boolean>;
  persistSession(response: LoginResponse): void;
  getSessionToken(): string | undefined;
  getLoggedUser(): User | undefined;
}
