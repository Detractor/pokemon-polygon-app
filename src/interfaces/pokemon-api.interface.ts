import type { PokemonResponse } from "@/dtos/pokemon/pokemon-response.dto";
import type { PokemonDetailsType } from "@/types/pokemon-types";

export interface PokemonApiInterface {
  getPokemons(page: number): Promise<PokemonResponse>;
  getPokemon(pokemonId: number): Promise<PokemonDetailsType>;
}
