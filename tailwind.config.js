/** @type {import('tailwindcss').Config} */
/* eslint-env node */
module.exports = {
  content: ["./src/**/*.{vue,ts,html}", "./index.html"],
  theme: {
    extend: {},
  },
  plugins: [],
};
